-- start main query ----------

SELECT
 t1.log_id,
 t1.log_loc_id as id_location,
  t1.log_com_id as id_comp,
  log_value as val,
  log_datetime as dtime,
  loc_id,
  loc_name as location,
   com_id,
   com_name as component,
    com_pdc
 FROM
 tbl_log t1,
 (SELECT
  log_loc_id, log_com_id, MAX(log_datetime) dtime
  FROM tbl_log
    WHERE
      log_datetime<'2019-05-10 11:37:52'
  GROUP BY
  log_loc_id, log_com_id) AS t2,
  tbl_locations AS t3,
  tbl_components AS t4
WHERE
t1.log_loc_id=t2.log_loc_id
AND
 t1.log_datetime=t2.dtime
 AND
  t1.log_com_id=t2.log_com_id
  AND
  t3.loc_id=t1.log_loc_id
  AND
  t4.com_id=t1.log_com_id
  ORDER BY
  location, component

-- start main query ----------

-- start main query ----------

SELECT
 t1.log_id, t1.log_loc_id as id_location, t1.log_com_id as id_comp, log_value as val, log_datetime as dtime, loc_id, loc_name as location, com_id, com_name as component, com_pdc, t5.total
 FROM
 tbl_log t1,
 (SELECT
  log_loc_id, log_com_id, MAX(log_datetime) dtime
  FROM tbl_log
    WHERE
      log_datetime<'2019-05-25 11:37:52'
  GROUP BY
  log_loc_id, log_com_id) AS t2,
  tbl_locations AS t3,
  tbl_components AS t4,
  (SELECT
    log_loc_id,
    log_com_id,
    SUM(log_value) as total
    FROM
    tbl_log
    WHERE
      log_datetime<'2019-05-25 11:37:52'
    GROUP BY
    log_com_id,
    log_loc_id
    ) as t5
WHERE
t1.log_loc_id=t2.log_loc_id
AND
 t1.log_datetime=t2.dtime
 AND
  t1.log_com_id=t2.log_com_id
  AND
  t3.loc_id=t1.log_loc_id
  AND
  t4.com_id=t1.log_com_id
  AND
  t1.log_loc_id=t5.log_loc_id
  AND
  t1.log_com_id=t5.log_com_id
  ORDER BY
  location, component

-- start main query ----------

SELECT
    t1.loc_id id_location,
    t2.com_id id_comp,
    t1.loc_name location,
    t2.com_name component,
    MAX(t3.log_id) id,
    MAX(t3.log_value) val,
    COALESCE(MAX(t3.log_datetime), 0) dtime
FROM
( tbl_locations t1,
       tbl_components t2 )
LEFT JOIN
tbl_log t3
    ON
     t1.loc_id = t3.log_loc_id
   AND
    t2.com_id = t3.log_com_id
    AND
    t3.log_datetime<now()
GROUP BY
 t1.loc_id,
 t2.com_id

---------------------


SELECT
COALESCE(bt.log_id, 0) id,
st.component,
st.location,
COALESCE(bt.log_com_id, 0) id_comp,
COALESCE(bt.log_loc_id, 0) id_location,
COALESCE(bt.log_value, 0) val,
st.pdc,
COALESCE(bt.log_datetime, 0) dtime
-- bt.*
 FROM
tbl_log bt
RIGHT JOIN
(SELECT
    t1.loc_id,
    t2.com_id,
    t1.loc_name location,
    t2.com_name component,
    t2.com_pdc pdc,
    MAX(t3.log_id) id,
    COALESCE(MAX(t3.log_datetime), 0) dtime
FROM
(( tbl_locations t1,
       tbl_components t2 )
LEFT JOIN
tbl_log t3
    ON
     t1.loc_id = t3.log_loc_id
   AND
    t2.com_id = t3.log_com_id
    AND
--     t3.log_datetime<'2019-05-10 11:37:52')
    t3.log_datetime<now())
    LEFT JOIN
    (SELECT
    t4.log_id id,
    t4.log_value val,
    t4.log_datetime dt
    FROM
    tbl_log t4) t5
    ON
    t5.id = t3.log_id
GROUP BY
 t1.loc_id,
 t2.com_id) st
 ON
 st.id=bt.log_id
ORDER BY
component,
location,
pdc



SELECT
COALESCE(bt.log_id, 0) id,
st.component,
st.location,
COALESCE(bt.log_com_id, 0) id_comp,
COALESCE(bt.log_loc_id, 0) id_location,
COALESCE(bt.log_value, 0) val,
st.pdc,
COALESCE(bt.log_datetime, 0) dtime,
t6.total
-- bt.*
 FROM
tbl_log bt
RIGHT JOIN
(SELECT
    t1.loc_id,
    t2.com_id,
    t1.loc_name location,
    t2.com_name component,
    t2.com_pdc pdc,
    MAX(t3.log_id) id,
    COALESCE(MAX(t3.log_datetime), 0) dtime
FROM
(( tbl_locations t1,
       tbl_components t2 )
LEFT JOIN
tbl_log t3
    ON
     t1.loc_id = t3.log_loc_id
   AND
    t2.com_id = t3.log_com_id
    AND
    t3.log_datetime<'2019-05-12 11:37:52'
    AND
    t3.log_datetime>'2019-05-10 11:37:52')
--     t3.log_datetime<'2019-05-10 11:37:52')
--     t3.log_datetime<now())
    LEFT JOIN
    (SELECT
    t4.log_id id,
    t4.log_value val,
    t4.log_datetime dt
    FROM
    tbl_log t4) t5
    ON
    t5.id = t3.log_id
GROUP BY
 t1.loc_id,
 t2.com_id) st
 ON
 st.id=bt.log_id
 LEFT JOIN
  (select
t.log_loc_id loc,
t.log_com_id comp,
sum(t.log_value) total
 from
 tbl_log t
 WHERE
  t.log_datetime<'2019-05-12 11:37:52'
    AND
    t.log_datetime>'2019-05-10 11:37:52'
 GROUP BY
 loc,comp) t6
 ON
 t6.comp = st.com_id
 AND
 t6.loc = st.loc_id
ORDER BY
component,
location,
pdc


