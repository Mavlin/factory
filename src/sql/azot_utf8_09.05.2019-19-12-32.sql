-- MySQL dump 10.13  Distrib 5.7.18, for Win64 (x86_64)
--
-- Host: localhost    Database: azot
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_components`
--

DROP TABLE IF EXISTS `tbl_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_components` (
  `com_id` int(11) NOT NULL AUTO_INCREMENT,
  `com_name` text NOT NULL,
  `com_pdc` int(11) NOT NULL DEFAULT '0',
  `com_main` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`com_id`),
  UNIQUE KEY `Uniq` (`com_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_components`
--

LOCK TABLES `tbl_components` WRITE;
/*!40000 ALTER TABLE `tbl_components` DISABLE KEYS */;
INSERT INTO `tbl_components` VALUES (1,'хлор',10,1),(2,'водород',20,0),(3,'СО',30,0),(4,'метан',40,0);
/*!40000 ALTER TABLE `tbl_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_locations`
--

DROP TABLE IF EXISTS `tbl_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_locations` (
  `loc_id` int(11) NOT NULL AUTO_INCREMENT,
  `loc_name` text NOT NULL,
  PRIMARY KEY (`loc_id`),
  UNIQUE KEY `Uniq` (`loc_name`(100))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_locations`
--

LOCK TABLES `tbl_locations` WRITE;
/*!40000 ALTER TABLE `tbl_locations` DISABLE KEYS */;
INSERT INTO `tbl_locations` VALUES (1,'цех №1'),(2,'цех №2'),(3,'цех №3'),(4,'цех №4'),(5,'цех №5');
/*!40000 ALTER TABLE `tbl_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_log`
--

DROP TABLE IF EXISTS `tbl_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_loc_id` int(11) NOT NULL,
  `log_com_id` int(11) NOT NULL,
  `log_value` int(11) NOT NULL DEFAULT '0',
  `log_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`),
  KEY `FK_tbl_log_tbl_locations` (`log_loc_id`),
  KEY `FK_tbl_log_tbl_components` (`log_com_id`),
  CONSTRAINT `FK_tbl_log_tbl_components` FOREIGN KEY (`log_com_id`) REFERENCES `tbl_components` (`com_id`),
  CONSTRAINT `FK_tbl_log_tbl_locations` FOREIGN KEY (`log_loc_id`) REFERENCES `tbl_locations` (`loc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_log`
--

LOCK TABLES `tbl_log` WRITE;
/*!40000 ALTER TABLE `tbl_log` DISABLE KEYS */;
INSERT INTO `tbl_log` VALUES (1,1,1,8,'2019-05-09 04:37:52'),(2,2,1,9,'2019-05-09 04:37:52'),(3,3,1,7,'2019-05-09 04:37:52'),(4,4,1,5,'2019-05-09 04:37:52'),(5,5,1,6,'2019-05-09 04:37:52'),(6,1,2,10,'2019-05-09 04:37:52'),(7,2,2,12,'2019-05-09 04:37:52'),(8,3,2,30,'2019-05-09 04:37:52'),(9,4,2,33,'2019-05-09 04:37:52'),(10,5,2,61,'2019-05-09 04:37:52'),(11,1,3,15,'2019-05-09 04:37:52'),(12,2,3,16,'2019-05-09 04:37:52'),(13,3,3,22,'2019-05-09 04:37:52'),(14,4,3,41,'2019-05-09 04:37:52'),(15,5,3,26,'2019-05-09 04:37:52'),(16,1,4,18,'2019-05-09 04:37:52'),(17,2,4,32,'2019-05-09 04:37:52'),(18,3,4,27,'2019-05-09 04:37:52'),(19,4,4,91,'2019-05-09 04:37:52'),(20,4,4,95,'2019-05-10 04:37:52'),(21,5,4,44,'2019-05-10 04:37:52');
/*!40000 ALTER TABLE `tbl_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-09 19:12:33
