            SELECT
            log_com_id as id_comp,
            log_loc_id as id_location,
            max(log.log_datetime) as dtime,
            tbl_locations.loc_name as location,
            tbl_components.com_name as component,
            COALESCE(total, 0) total,
            tbl_components.com_quota quota
            from
            (tbl_log log,
            tbl_locations,
            tbl_components)
            LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            COALESCE(sum(t.log_value), 0) total
             from
             tbl_log t
             WHERE
              t.log_datetime>'2019-05-05'
              and
                t.log_loc_id=2
                and
                t.log_com_id=2
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = log.log_com_id
             AND
             t6.loc = log.log_loc_id

            WHERE
            tbl_locations.loc_id=log.log_loc_id
            and
            log.log_com_id=tbl_components.com_id
                          and
                log.log_loc_id=2
                and
                log.log_com_id=2
            GROUP by
            log_com_id,
            log_loc_id,
            loc_name,
            com_name
            ORDER by
            log.log_com_id;


SELECT
            COALESCE(bt.log_id, 0) id,
            st.component,
            st.location,
            COALESCE(st.com_id, 0) id_comp,
            COALESCE(st.loc_id, 0) id_location,
            COALESCE(bt.log_value, 0) val,
            st.pdc,
            st.quota,
            COALESCE(bt.log_datetime, 0) dtime,
            COALESCE(t6.total, 0) total
             FROM
            tbl_log bt
            RIGHT JOIN
            (SELECT
                t1.loc_id,
                t2.com_id,
                t1.loc_name location,
                t2.com_name component,
                t2.com_pdc pdc,
                t2.com_quota quota,
                MAX(t3.log_id) id,
                COALESCE(MAX(t3.log_datetime), 0) dtime
            FROM
            (( tbl_locations t1,
                   tbl_components t2 )
            LEFT JOIN
            tbl_log t3
                ON
                 t1.loc_id = t3.log_loc_id
               AND
                t2.com_id = t3.log_com_id
                AND
                t3.log_datetime>'2019-05-05 11:37:52')
                LEFT JOIN
                (SELECT
                t4.log_id id,
                t4.log_value val,
                t4.log_datetime dt
                FROM
                tbl_log t4) t5
                ON
                t5.id = t3.log_id
            GROUP BY
             t1.loc_id,
             t2.com_id) st
             ON
             st.id=bt.log_id
             LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            COALESCE(sum(t.log_value), 0) total
             from
             tbl_log t
             WHERE
              t.log_datetime>'2019-05-05 11:37:52'
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = st.com_id
             AND
             t6.loc = st.loc_id
            ORDER BY
            component,
            location,
            quota,
            pdc;




                SELECT
                COALESCE(bt.log_id, 0) id,
                st.component,
                st.location,
                COALESCE(st.com_id, 0) id_comp,
                COALESCE(st.loc_id, 0) id_location,
                COALESCE(bt.log_value, 0) val,
                st.pdc,
                COALESCE(bt.log_datetime, 0) dtime
--                 t6.total
                 FROM
                tbl_log bt
                RIGHT JOIN
                (SELECT
                    t1.loc_id,
                    t2.com_id,
                    t1.loc_name location,
                    t2.com_name component,
                    t2.com_pdc pdc,
                    MAX(t3.log_id) id,
                    COALESCE(MAX(t3.log_datetime), 0) dtime
                FROM
                (( tbl_locations t1,
                       tbl_components t2 )
                LEFT JOIN
                tbl_log t3
                    ON
                     t1.loc_id = t3.log_loc_id
                   AND
                    t2.com_id = t3.log_com_id
                    AND
                    t3.log_datetime<now())
                    LEFT JOIN
                    (SELECT
                    t4.log_id id,
                    t4.log_value val,
                    t4.log_datetime dt
                    FROM
                    tbl_log t4) t5
                    ON
                    t5.id = t3.log_id
                GROUP BY
                 t1.loc_id,
                 t2.com_id) st
                 ON
                 st.id=bt.log_id
                 LEFT JOIN
                  (select
                t.log_loc_id loc,
                t.log_com_id comp
                 from
                 tbl_log t
                 WHERE
                  t.log_datetime<now()
                 GROUP BY
                 loc,comp) t6
                 ON
                 t6.comp = st.com_id
                 AND
                 t6.loc = st.loc_id
                ORDER BY
                component,
                location,
                pdc;

-------------------

select
t.log_loc_id loc,
t.log_com_id comp,
sum(t.log_value)summ
 from
 tbl_log t
 GROUP BY
 loc,
 comp


SELECT
log_com_id as id_comp,
log_loc_id as id_location,
max(log_value) as val,
max(log.log_datetime) as dtime,
tbl_locations.loc_name as location,
tbl_components.com_name as component,
tbl_components.com_pdc as pdc,
total,
tbl_components.com_quota
from
(tbl_log log,
tbl_locations,
tbl_components)
LEFT JOIN
  (select
t.log_loc_id loc,
t.log_com_id comp,
sum(t.log_value) total
 from
 tbl_log t
 WHERE
  t.log_datetime>'2019-05-05'
  and
    t.log_loc_id=5
    and
    t.log_com_id=2

--   AND
--   t.log_com_id = tbl_log.log_com_id
--   AND
--   t.log_loc_id = tbl_log.log_loc_id
--     AND
--     t.log_datetime>'2019-05-05 11:37:52'
 GROUP BY
 loc,comp) t6
 ON
 t6.comp = log.log_com_id
 AND
 t6.loc = log.log_loc_id

WHERE
tbl_locations.loc_id=log.log_loc_id
and
log.log_com_id=tbl_components.com_id
and
-- t6.comp=2
-- and
-- t6.loc=5
-- and
log.log_id=30
--     AND
--     log.log_datetime>'2019-05-10 11:37:52'

GROUP by
log_com_id,
log_loc_id,
loc_name,
com_name,
com_pdc
ORDER by
log.log_com_id



select
t.log_com_id comp,
t.log_loc_id loc,
sum(t.log_value) total
 from
 tbl_log t
 WHERE
--   t.log_datetime<'2019-05-20 11:37:52'
--     AND
    t.log_datetime>'2019-05-05 11:37:52'
 GROUP BY
 loc,comp






