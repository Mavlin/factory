import Vue from 'vue'
// import Vuex from 'vuex'
import VueRouter from 'vue-router'
import LightBootstrap from './light-bootstrap-main'

import "./assets/main.scss";
import 'nprogress/nprogress.css'
import NProgress from 'nprogress'
// Plugins
import App from './App.vue'

// import * as moment from 'moment';
// moment.locale('ru-RU');

// router setup
import routes from './routes/routes'
// plugin setup
Vue.use(VueRouter)
// Vue.use(Vuex)
Vue.use(LightBootstrap)

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    linkActiveClass: 'active'
})

// NProgress.start()

router.beforeResolve((to, from, next) => {
    // If this isn't an initial page load.
    if (to.name) {
        // Start the route progress bar.
        NProgress.start()
        // console.log(777)
    }
    next()
})

router.afterEach((to, from) => {
    // console.log(888)
    // Complete the animation of the route progress bar.
    NProgress.done()
    // NProgress.remove();
})


/* eslint-disable no-new */

// let user={login: 'mavlin', pass: 123}
// const store = new Vuex.Store({
//     state: {
//         currentUser: null
//     },
//     mutations: {
//         setCurrentUser(currentState, user) {
//             currentState.currentUser = user;
//         }
//     },
//     actions: {
//         login(context, credentials) {
//             return store.commit('setCurrentUser', user).then(() => {
//                 console.log('credentials')
//             })
//             // return myLoginApi.post(credentials).then((user) => {
//             //     context.commit('setCurrentUser', user)
//         // }
//         }
//     },
//     getters: {
//         user(state) {
//             return state.currentUser
//         }
//     }
// })
// // store.commit('setCurrentUser', user);
// window.Bus = {
//     add(arg){
//         return new Promise(function (resolve, reject) {
//             arg()
//             resolve()
//         })
//     }
// };
// console.log(new Date())
import {store} from './store';

new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router
})
