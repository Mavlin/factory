/* eslint-disable */

function run(el) {

    /**
     * составной объект для хранения информации о переносе:
     * {
   *   elem - элемент, на котором была зажата мышь
   *   avatar - аватар
   *   downX/downY - координаты, на которых был mousedown
   *   shiftX/shiftY - относительный сдвиг курсора от угла элемента
   * }
     */
    let dragObject = {};

    let self = this;

    function onMouseDown(el) {

        if (el.which !== 1) return;

        let elem = el.target.closest('.draggable');

        // console.log(elem)
        if (!elem) return;

        dragObject.elem = elem;

        // console.log(dragObject.elem)
        // dragObject.elem.style.color = "red"
        // запомним, что элемент нажат на текущих координатах pageX/pageY
        dragObject.downX = el.pageX;
        dragObject.downY = el.pageY;

        return false;
    }

    function onMouseMove(el) {
        if (!dragObject.elem) return; // элемент не зажат

        if (!dragObject.avatar) { // если перенос не начат...
            let moveX = el.pageX - dragObject.downX;
            let moveY = el.pageY - dragObject.downY;

            // если мышь передвинулась в нажатом состоянии недостаточно далеко
            if (Math.abs(moveX) < 3 && Math.abs(moveY) < 3) {
                return;
            }

            // начинаем перенос

            dragObject.avatar = createAvatar(el); // создать аватар
            if (!dragObject.avatar) { // отмена переноса, нельзя "захватить" за
                //эту часть элемента
                dragObject = {};
                return;
            }

            // аватар создан успешно
            // создать вспомогательные свойства shiftX/shiftY
            let coords = getCoords(dragObject.avatar);

            dragObject.shiftX = dragObject.downX - coords.left;
            dragObject.shiftY = dragObject.downY - coords.top;

            startDrag(el); // отобразить начало переноса
        }

        // отобразить перенос объекта при каждом движении мыши
        dragObject.avatar.style.left = el.pageX - dragObject.shiftX + 'px';
        dragObject.avatar.style.top = el.pageY - dragObject.shiftY + 'px';

        return false;
    }

    function onMouseUp(el) {
        // debugger
        if (dragObject.avatar) { // если перенос идет
            // console.log(dragObject.elem)
            // dragObject.elem.style.left = ""
            // dragObject.elem.style.color = "red"

            finishDrag(el);
        }


        // перенос либо не начинался, либо завершился
        // в любом случае очистим "состояние переноса" dragObject
        dragObject = {};
    }

    function finishDrag(el) {
        let dropElem = findDroppable(el);

        // if (!dropElem) {
        //     self.onDragCancel(dragObject);
        // } else {
        //     self.onDragEnd(dragObject, dropElem);
        // }
    }

    function createAvatar() {

        // запомнить старые свойства, чтобы вернуться к ним при отмене переноса
        let avatar = dragObject.elem;
        let old = {
            parent: avatar.parentNode,
            nextSibling: avatar.nextSibling,
            position: avatar.position || '',
            left: avatar.left || '',
            top: avatar.top || '',
            zIndex: avatar.zIndex || ''
        };

        // функция для отмены переноса
        avatar.rollback = function () {
            old.parent.insertBefore(avatar, old.nextSibling);
            avatar.style.position = old.position;
            avatar.style.left = old.left;
            avatar.style.top = old.top;
            avatar.style.zIndex = old.zIndex
        };

        return avatar;
    }

    function startDrag() {
        let avatar = dragObject.avatar;

        // инициировать начало переноса
        // document.getElementById('content').appendChild(avatar);
        // console.log(document.getElementById('content'))
        // console.log(el.parentNode)
        // console.log(el.parentNode.parentNode)
        // console.log(el.parentNode.parentNode.parentNode)
        // console.log(el.parentNode.parentNode.parentNode.parentNode)
        // console.log(el.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode)
        // document.body.appendChild(avatar);
        avatar.style.zIndex = 9000;
        avatar.style.position = 'absolute';
    }

    function findDroppable(event) {
        // спрячем переносимый элемент
        dragObject.avatar.hidden = true;

        // получить самый вложенный элемент под курсором мыши
        let elem = document.elementFromPoint(event.clientX, event.clientY);

        // показать переносимый элемент обратно
        dragObject.avatar.hidden = false;

        if (elem === null) {
            // такое возможно, если курсор мыши "вылетел" за границу окна
            return null;
        }

        return elem.closest('.droppable');
    }

    // el.onmousemove = onMouseMove;
    // el.onmouseup = onMouseUp;
    // el.onmousedown = onMouseDown;

    document.onmousemove = onMouseMove;
    document.onmouseup = onMouseUp;
    document.onmousedown = onMouseDown;

    // this.onDragEnd = function (dragObject, dropElem) {
    // };
    // this.onDragCancel = function (dragObject) {
    // };

}


function getCoords(elem) { // кроме IE8-
    let box = elem.getBoundingClientRect();
    // console.log(box)
    return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
    }
}

export {run}
