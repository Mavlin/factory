import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        switchTotal: true,
        period: 2,
        datePicker: new Date(),
        // switchTotal2: true
    },
    getters: {},
    mutations: {
        SET_SWITCH_TOTAL: (state, payload)=>{
            state.switchTotal = payload
        },
        SET_PERIOD:(state, payload)=>{
            state.period = payload
        },
        SET_PICKER:(state, payload)=>{
            state.datePicker = payload
        },
    },
    actions: {
        SET_TOTAL (context, payload) {
            context.commit('SET_SWITCH_TOTAL', payload)
        },
        SET_PERIOD(context, payload){
            context.commit('SET_PERIOD', payload)
        },
        SET_PICKER(context, payload){
            context.commit('SET_PICKER', payload)
        }
    }
});
