<?php

namespace Controllers;

class WorkLog extends Controller //
{
    protected $WorkLog; // ссылка на модель

    public function __construct($req)
    {
        parent::setCfg();
        $this->WorkLog = new \Models\WorkLog($this->config);
        if ($req !== null) {
            parent::__construct($req);
        }
    }

    public function getDataWithTotal()
    {
// http://streletzcoder.ru/slozhnaya-vyiborka-poslednih-dannyih-po-date-iz-odnoy-tablitsyi-na-primere-microsoft-sql-server/
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $id_log = $this->getRequestParam($this->request, 'id_log');
        $id_comp = $this->getRequestParam($this->request, 'id_comp');
        $id_loc = $this->getRequestParam($this->request, 'id_loc');
        $dateLimit = $this->getRequestParam($this->request, 'date');
        $result = [];
        if (!empty($id_comp)) { //
            $result = $this->WorkLog->getDataWithTotal($id_comp, $id_loc, $dateLimit, $id_log);
        }
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }


    public function getTotal()
    {
// http://streletzcoder.ru/slozhnaya-vyiborka-poslednih-dannyih-po-date-iz-odnoy-tablitsyi-na-primere-microsoft-sql-server/
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $id_comp = $this->getRequestParam($this->request, 'id_comp');
        $id_loc = $this->getRequestParam($this->request, 'id_loc');
        $dateLimit = $this->getRequestParam($this->request, 'date');
        $result = [];
        if (!empty($id_comp)) { //
            $result = $this->WorkLog->getTotal($id_comp, $id_loc, $dateLimit);
        }
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

    public function getAllDataInPeriod() // для второй страницы - возвращает суммарные выбросы
    {
// http://streletzcoder.ru/slozhnaya-vyiborka-poslednih-dannyih-po-date-iz-odnoy-tablitsyi-na-primere-microsoft-sql-server/
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $date = $this->getRequestParam($this->request, 'date');
        $result = [];
        if (!empty($date)) { //
            $result = $this->WorkLog->getAllDataInPeriod($date);
        }
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

    public function getAllData()
    {
        $result = $this->WorkLog->getAllData();
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

    public function getAllDataTotal()
    {
        $this->decodeRequest($this->request[(string)__FUNCTION__]['data']);
        $date = $this->getRequestParam($this->request, 'date');
//        $result = [];

        $result = $this->WorkLog->getAllDataTotal($date);
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

    public function getAllComponents()
    {
        $result = [];
        if (!empty($date)) { //
            $result = $this->WorkLog->getAllComponents();
        }
        if ($result) {
            $responseCode = $this->success;
            $this->setResponse('data', $result);
        } else {
            $responseCode = $this->SomethingWrong;
        }
        $this->setResponse('message', $responseCode['message']);
        $this->setResponse('status', 'ok');
        $this->setResponse('code', $responseCode['code']);
    }

}
