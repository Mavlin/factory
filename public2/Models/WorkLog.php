<?php

namespace Models;

class WorkLog extends Model
{

    public function __construct($request = null)
    {
        parent::__construct($request);
    }

    // возвращает подробные данные для конкретной (одной) записи из tbl_log и накопленные выбросы по данному компоненту в данной локации
    public function getData($id_comp, $upLimitDate, $lowLimitDate)
    {
        $sql = "
            SELECT
            log_com_id as id_comp,
            log_loc_id as id_location,
            max(log_value) as val,
            max(log.log_datetime) as dtime,
            tbl_locations.loc_name as location,
            tbl_components.com_name as component,
            tbl_components.com_pdc as pdc,
            total,
            tbl_components.com_quota
            from
            (tbl_log log,
            tbl_locations,
            tbl_components)
            LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            sum(t.log_value) total
             from
             tbl_log t
             WHERE
              t.log_datetime<?s
                AND
                t.log_datetime>?s
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = log.log_com_id
             AND
             t6.loc = log.log_loc_id
            
            WHERE
            tbl_locations.loc_id=log.log_loc_id
            and
            log.log_com_id=tbl_components.com_id
            and
            log.log_id=?i
            GROUP by
            log_com_id,
            log_loc_id,
            loc_name,
            com_name,
            com_pdc
            ORDER by
            log.log_com_id
        ";
        return $this->connect->getAll($sql, $upLimitDate, $lowLimitDate, $id_comp);
    }

    // возвращает подробные данные для конкретной (одной) записи из tbl_log и накопленные выбросы по данному компоненту в данной локации c определенной даты
    public function getDataWithTotal($id_comp, $id_loc, $dateLimit, $id_log)
    {
        $sql = "
            SELECT
            log_com_id as id_comp,
            log_loc_id as id_location,
            max(log_value) as val,
            max(log.log_datetime) as dtime,
            tbl_locations.loc_name as location,
            tbl_components.com_name as component,
            tbl_components.com_pdc as pdc,
            COALESCE(total, 0) total,
            tbl_components.com_quota quota 
            from
            (tbl_log log,
            tbl_locations,
            tbl_components)
            LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            COALESCE(sum(t.log_value), 0) total
             from
             tbl_log t
             WHERE
              t.log_datetime>?s
              and
                t.log_loc_id=?s
                and
                t.log_com_id=?s
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = log.log_com_id
             AND
             t6.loc = log.log_loc_id
            
            WHERE
            tbl_locations.loc_id=log.log_loc_id
            and
            log.log_com_id=tbl_components.com_id
            and
            log.log_id=?s
            GROUP by
            log_com_id,
            log_loc_id,
            loc_name,
            com_name,
            com_pdc
            ORDER by
            log.log_com_id
        ";
        return $this->connect->getAll($sql, $dateLimit, $id_loc, $id_comp, $id_log);
    }

    // возвращает подробные накопленные выбросы по данному компоненту в данной локации c определенной даты
    public function getTotal($id_comp, $id_loc, $dateLimit)
    {
        $sql = "
            SELECT
            log_com_id as id_comp,
            log_loc_id as id_location,
            max(log.log_datetime) as dtime,
            tbl_locations.loc_name as location,
            tbl_components.com_name as component,
            COALESCE(total, 0) total,
            tbl_components.com_quota quota 
            from
            (tbl_log log,
            tbl_locations,
            tbl_components)
            LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            COALESCE(sum(t.log_value), 0) total
             from
             tbl_log t
             WHERE
              t.log_datetime>?s
              and
                t.log_loc_id=?s
                and
                t.log_com_id=?s
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = log.log_com_id
             AND
             t6.loc = log.log_loc_id
            
            WHERE
            tbl_locations.loc_id=log.log_loc_id
            and
            log.log_com_id=tbl_components.com_id
              and
                log.log_loc_id=?s
                and
                log.log_com_id=?s
            GROUP by
            log_com_id,
            log_loc_id,
            loc_name,
            com_name
            ORDER by
            log.log_com_id
        ";
        return $this->connect->getAll($sql, $dateLimit, $id_loc, $id_comp, $id_loc, $id_comp);
    }

    // получим WorkLog (срез данных на дату)
    public function getAllData()
    {
        $sql = "
                SELECT
                COALESCE(bt.log_id, 0) id,
                st.component,
                st.location,
                COALESCE(st.com_id, 0) id_comp,
                COALESCE(st.loc_id, 0) id_location,
                COALESCE(bt.log_value, 0) val,
                st.pdc,
                COALESCE(bt.log_datetime, 0) dtime
                 FROM
                tbl_log bt
                RIGHT JOIN
                (SELECT
                    t1.loc_id,
                    t2.com_id,
                    t1.loc_name location,
                    t2.com_name component,
                    t2.com_pdc pdc,
                    MAX(t3.log_id) id,
                    COALESCE(MAX(t3.log_datetime), 0) dtime
                FROM
                (( tbl_locations t1,
                       tbl_components t2 )
                LEFT JOIN
                tbl_log t3
                    ON
                     t1.loc_id = t3.log_loc_id
                   AND
                    t2.com_id = t3.log_com_id
                    AND
                    t3.log_datetime<now())
                    LEFT JOIN
                    (SELECT
                    t4.log_id id,
                    t4.log_value val,
                    t4.log_datetime dt
                    FROM
                    tbl_log t4) t5
                    ON
                    t5.id = t3.log_id
                GROUP BY
                 t1.loc_id,
                 t2.com_id) st
                 ON
                 st.id=bt.log_id
                 LEFT JOIN
                  (select
                t.log_loc_id loc,
                t.log_com_id comp
                 from
                 tbl_log t
                 WHERE
                  t.log_datetime<now()
                 GROUP BY
                 loc,comp) t6
                 ON
                 t6.comp = st.com_id
                 AND
                 t6.loc = st.loc_id
                ORDER BY
                component,
                location,
                pdc
              ";

        return $this->connect->getAll($sql);
    }

    // получим WorkLog (данные с накоплением с определенной даты)
    public function getAllDataTotal($date)
    {
        $sql = "
            SELECT
            COALESCE(bt.log_id, 0) id,
            st.component,
            st.location,
            COALESCE(st.com_id, 0) id_comp,
            COALESCE(st.loc_id, 0) id_location,
            COALESCE(bt.log_value, 0) val,
            st.pdc,
            COALESCE(bt.log_datetime, 0) dtime,
            COALESCE(t6.total, 0) total
             FROM
            tbl_log bt
            RIGHT JOIN
            (SELECT
                t1.loc_id,
                t2.com_id,
                t1.loc_name location,
                t2.com_name component,
                t2.com_pdc pdc,
                MAX(t3.log_id) id,
                COALESCE(MAX(t3.log_datetime), 0) dtime
            FROM
            (( tbl_locations t1,
                   tbl_components t2 )
            LEFT JOIN
            tbl_log t3
                ON
                 t1.loc_id = t3.log_loc_id
               AND
                t2.com_id = t3.log_com_id
                AND
                t3.log_datetime<now())
                LEFT JOIN
                (SELECT
                t4.log_id id,
                t4.log_value val,
                t4.log_datetime dt
                FROM
                tbl_log t4) t5
                ON
                t5.id = t3.log_id
            GROUP BY
             t1.loc_id,
             t2.com_id) st
             ON
             st.id=bt.log_id
             LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            COALESCE(sum(t.log_value), 0) total
             from
             tbl_log t
             WHERE
              t.log_datetime>?s
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = st.com_id
             AND
             t6.loc = st.loc_id
            ORDER BY
            component,
            location,
            pdc;
              ";

        return $this->connect->getAll($sql, $date);
    }

    // получим WorkLog (данные с накоплением с определенной даты = начало определенного периода)
    public function getAllDataInPeriod($date)
    {
        $sql = "
          SELECT
            COALESCE(bt.log_id, 0) id,
            st.component,
            st.location,
            COALESCE(st.com_id, 0) id_comp,
            COALESCE(st.loc_id, 0) id_location,
            st.quota quota,
            COALESCE(bt.log_datetime, 0) dtime,
            COALESCE(t6.total, 0) total
             FROM
            tbl_log bt
            RIGHT JOIN
            (SELECT
                t1.loc_id,
                t2.com_id,
                t1.loc_name location,
                t2.com_name component,
                t2.com_quota quota,
                MAX(t3.log_id) id,
                COALESCE(MAX(t3.log_datetime), 0) dtime
            FROM
            (( tbl_locations t1,
                   tbl_components t2 )
            LEFT JOIN
            tbl_log t3
                ON
                 t1.loc_id = t3.log_loc_id
               AND
                t2.com_id = t3.log_com_id
                AND
                t3.log_datetime>?s)
                LEFT JOIN
                (SELECT
                t4.log_id id,
                t4.log_value val,
                t4.log_datetime dt
                FROM
                tbl_log t4) t5
                ON
                t5.id = t3.log_id
            GROUP BY
             t1.loc_id,
             t2.com_id) st
             ON
             st.id=bt.log_id
             LEFT JOIN
              (select
            t.log_loc_id loc,
            t.log_com_id comp,
            COALESCE(sum(t.log_value), 0) total
             from
             tbl_log t
             WHERE
              t.log_datetime>?s
             GROUP BY
             loc,comp) t6
             ON
             t6.comp = st.com_id
             AND
             t6.loc = st.loc_id
            ORDER BY
            component,
            location,
            quota
            ;
              ";

        return $this->connect->getAll($sql, $date, $date);
    }

    // получим WorkLog
    public function getData2($id_comp)
    {
        $sql = "SELECT log_com_id as id_comp, log_loc_id as id_location, max(log_value) as val, max(tbl_log.log_datetime) as dtime, tbl_locations.loc_name as location, tbl_components.com_name as component, tbl_components.com_pdc as pdc from tbl_log, tbl_locations, tbl_components WHERE tbl_locations.loc_id=tbl_log.log_loc_id and tbl_log.log_com_id=tbl_components.com_id and tbl_components.com_id=?i GROUP by log_com_id, log_loc_id, loc_name, com_name, com_pdc ORDER by tbl_log.log_com_id";
        return $this->connect->getAll($sql, $id_comp);
    }

    public function getAllComponents()
    {
        $sql = "SELECT * FROM tbl_components;";
        return $this->connect->getAll($sql);
    }


}

